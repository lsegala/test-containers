# Test Containers

Projeto para testar a subida e destruição de uma imagem de banco durante a fase de teste

## Installation

```bash
mvn package
```

## Usage

```bash
java -jar target\<jar-name>.jar
```