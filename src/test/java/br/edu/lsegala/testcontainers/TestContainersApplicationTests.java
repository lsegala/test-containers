package br.edu.lsegala.testcontainers;

import br.edu.lsegala.testcontainers.entity.User;
import br.edu.lsegala.testcontainers.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
class TestContainersApplicationTests {
	@Autowired
	private UserRepository userRepository;

	@Test
	void mustSelectUser() {
		List<User> users = userRepository.findAll();
		assertNotNull(users);
	}

}
