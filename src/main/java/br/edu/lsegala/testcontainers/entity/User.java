package br.edu.lsegala.testcontainers.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("user")
public class User {
    @Id
    public String id;
    public String name;
}
