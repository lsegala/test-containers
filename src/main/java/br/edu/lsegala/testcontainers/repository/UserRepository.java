package br.edu.lsegala.testcontainers.repository;

import br.edu.lsegala.testcontainers.entity.User;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserRepository extends MongoRepository<User, String> {
}
